


findMatchesPlayedPerYear();
findMatchesWinPerTeam();
findExtraRunConcededPerTeam();
findTopBowlerOfAYear();
findBatsManRunForAYear();

function findMatchesPlayedPerYear() {

  const matchesPlayedByYear = new Map();

  const csv = require('csv-parser');
  const fs = require('fs');

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      if (matchesPlayedByYear.has(match.season)) {
        var matchesCount = matchesPlayedByYear.get(match.season);
        matchesPlayedByYear.delete(match.season);
        matchesPlayedByYear.set(match.season, matchesCount + 1);
      }
      else {
        matchesPlayedByYear.set(match.season, 1);
      }
    })
    .on('end', () => {
      console.log(matchesPlayedByYear);
    });
}


function findMatchesWinPerTeam() {

  const matchWinByTeam = new Map();

  const csv = require('csv-parser');
  const fs = require('fs');

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      if (matchWinByTeam.has(match.winner)) {
        var winCount = matchWinByTeam.get(match.winner);
        matchWinByTeam.delete(match.winner);
        matchWinByTeam.set(match.winner, winCount + 1);
      }
      else {
        matchWinByTeam.set(match.winner, 1);
      }
    })
    .on('end', () => {
      console.log(matchWinByTeam);
    });
}

function findExtraRunConcededPerTeam() {
  const extraRunsByTeam = new Map();
  const matchesID = new Set();

  const csv = require('csv-parser');
  const fs = require('fs');

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      if (match.season == "2016") {
        matchesID.add(match.id);
      }
    })
    .on('end', () => {
    });
  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/deliveries.csv')
    .pipe(csv())
    .on('data', (delivery) => {
      if (matchesID.has(delivery.match_id)) {
        if (extraRunsByTeam.has(delivery.batting_team)) {
          var extraRun = parseInt(extraRunsByTeam.get(delivery.batting_team));
          extraRunsByTeam.delete(delivery.batting_team);
          extraRunsByTeam.set(delivery.batting_team, extraRun + parseInt(delivery.extra_runs));
        }
        else {
          extraRunsByTeam.set(delivery.batting_team, delivery.extra_runs);
        }
      }
    })
    .on('end', () => {
      console.log(extraRunsByTeam);
    });

}

function findTopBowlerOfAYear() {
  const runGivenByBowler = new Map();
  const totalBallByBowler = new Map();
  const economyRateByBowler = new Map();
  const matchesID = new Set();

  var minEconomy = 100;
  var topBowler = "";
  const csv = require('csv-parser');
  const fs = require('fs');

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      if (match.season == "2015") {
        matchesID.add(match.id);
      }
    })
    .on('end', () => {
    });
  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/deliveries.csv')
    .pipe(csv())
    .on('data', (delivery) => {
      if (matchesID.has(delivery.match_id)) {
        if (runGivenByBowler.has(delivery.bowler)) {
          var runGiven = parseInt(runGivenByBowler.get(delivery.bowler));
          runGivenByBowler.delete(delivery.bowler);
          runGivenByBowler.set(delivery.bowler, runGiven + parseInt(delivery.total_runs));
        }
        else {
          runGivenByBowler.set(delivery.bowler, delivery.total_runs);
        }
        if (totalBallByBowler.has(delivery.bowler)) {
          var totalBall = totalBallByBowler.get(delivery.bowler);
          totalBallByBowler.delete(delivery.bowler);
          totalBallByBowler.set(delivery.bowler, totalBall + 1);
        }
        else {
          totalBallByBowler.set(delivery.bowler, 1);
        }
      }
    })
    .on('end', () => {
      const iterator = runGivenByBowler.keys();
      for (var i = 0; i < totalBallByBowler.size; i++) {
        var bowler = iterator.next().value;
        var economy = runGivenByBowler.get(bowler) / totalBallByBowler.get(bowler);
        economyRateByBowler.set(bowler, economy);
        if (minEconomy > economy) {
          minEconomy = economy;
          topBowler = bowler;
        }
      }
      console.log("Top bowler " + topBowler + " Economy Rate " + minEconomy);
    });
}
function findBatsManRunForAYear() {

  const runsByBatsman = new Map();
  const matchesID = new Set();

  const csv = require('csv-parser');
  const fs = require('fs');

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      if (match.season == "2008") {
        matchesID.add(match.id);
      }
    })
    .on('end', () => {
    });

  fs.createReadStream('/home/nesteen/IdeaProjects/IPLFinalProject/src/deliveries.csv')
    .pipe(csv())
    .on('data', (delivery) => {
      if (matchesID.has(delivery.match_id)) {
        if (runsByBatsman.has(delivery.batsman)) {
          var runGain = parseInt(runsByBatsman.get(delivery.batsman));
          runsByBatsman.delete(delivery.batsman);
          runsByBatsman.set(delivery.batsman, runGain + parseInt(delivery.batsman_runs));
        }
        else {
          runsByBatsman.set(delivery.batsman, delivery.batsman_runs);
        }
      }
    })
    .on('end', () => {
      console.log(runsByBatsman);
    });

}